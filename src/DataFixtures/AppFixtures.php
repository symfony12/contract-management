<?php

namespace App\DataFixtures;

use App\Entity\Department;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private UserPasswordEncoderInterface $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $dbmDep = new Department();
        $dbmDep->setCode('dep1');
        $dbmDep->setName('DBM');

        $dcDep = new Department();
        $dcDep->setCode('dep2');
        $dcDep->setName('DC');

        $dfDep = new Department();
        $dfDep->setCode('dep3');
        $dfDep->setName('DF');

        $dtDep = new Department();
        $dtDep->setCode('dep4');
        $dtDep->setName('DT');

        $drDep = new Department();
        $drDep->setCode('dep5');
        $drDep->setName('DR');

        $manager->persist($dbmDep);
        $manager->persist($dcDep);
        $manager->persist($dfDep);
        $manager->persist($dtDep);
        $manager->persist($drDep);

        $superadmin = new User();
        $superadmin->setEmail('superadmin@superadmin');
        $superadmin->setFirstName('superadmin');
        $superadmin->setLastName('superadmin');
        $superadmin->setPhoneNumber('+21699999999');
        $superadmin->setIsBlocked(false);
        $superadmin->setRoles(['ROLE_SUPER_ADMIN']);
        $superadmin->setPassword($this->passwordEncoder->encodePassword(
            $superadmin,
            'superadmin'
        ));

        $admin = new User();
        $admin->setEmail('admin@admin');
        $admin->setFirstName('admin');
        $admin->setLastName('admin');
        $admin->setPhoneNumber('+21699999999');
        $admin->setIsBlocked(false);
        $admin->setRoles(['ROLE_ADMIN']);
        $admin->setPassword($this->passwordEncoder->encodePassword(
            $admin,
            'admin'
        ));

        $dbm = new User();
        $dbm->setEmail('dbm@dbm');
        $dbm->setFirstName('dbm');
        $dbm->setLastName('dbm');
        $dbm->setPhoneNumber('+21699999999');
        $dbm->setIsBlocked(false);
        $dbm->setDepartment($dbmDep);
        $dbm->setRoles(['ROLE_DBM']);
        $dbm->setPassword($this->passwordEncoder->encodePassword(
            $dbm,
            'dbm'
        ));

        $dc = new User();
        $dc->setEmail('dc@dc');
        $dc->setFirstName('dc');
        $dc->setLastName('dc');
        $dc->setPhoneNumber('+21699999999');
        $dc->setIsBlocked(false);
        $dc->setDepartment($dcDep);
        $dc->setRoles(['ROLE_DC']);
        $dc->setPassword($this->passwordEncoder->encodePassword(
            $dc,
            'dc'
        ));

        $df = new User();
        $df->setEmail('df@df');
        $df->setFirstName('df');
        $df->setLastName('df');
        $df->setPhoneNumber('+21699999999');
        $df->setIsBlocked(false);
        $df->setDepartment($dfDep);
        $df->setRoles(['ROLE_DF']);
        $df->setPassword($this->passwordEncoder->encodePassword(
            $df,
            'df'
        ));

        $dt = new User();
        $dt->setEmail('dt@dt');
        $dt->setFirstName('dt');
        $dt->setLastName('dt');
        $dt->setPhoneNumber('+21699999999');
        $dt->setIsBlocked(false);
        $dt->setDepartment($dtDep);
        $dt->setRoles(['ROLE_USER']);
        $dt->setPassword($this->passwordEncoder->encodePassword(
            $dt,
            'dt'
        ));

        $dr = new User();
        $dr->setEmail('dr@dr');
        $dr->setFirstName('dr');
        $dr->setLastName('dr');
        $dr->setPhoneNumber('+21699999999');
        $dr->setIsBlocked(false);
        $dr->setDepartment($drDep);
        $dr->setRoles(['ROLE_USER']);
        $dr->setPassword($this->passwordEncoder->encodePassword(
            $dr,
            'dr'
        ));


        $manager->persist($superadmin);
        $manager->persist($admin);
        $manager->persist($dbm);
        $manager->persist($dc);
        $manager->persist($df);
        $manager->persist($dt);
        $manager->persist($dr);

        $manager->flush();
    }
}
