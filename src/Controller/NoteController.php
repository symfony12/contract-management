<?php

namespace App\Controller;

use App\Entity\Contract;
use App\Entity\Convention;
use App\Entity\Note;
use App\Entity\State;
use App\Entity\User;
use App\Form\NoteType;
use App\Service\FileUploader;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/note")
 */
class NoteController extends AbstractController
{
    const CREATED_TYPE = 'CREATED';
    const VALIDATED_TYPE = 'VALIDATED';
    const NOT_VALIDATED_TYPE = 'NOT_VALIDATED';
    const PAYED_TYPE = 'PAYED';
    const NOT_PAYED_TYPE = 'NOT_PAYED';

//    /**
//     * @Route("/", name="note_index", methods={"GET"})
//     */
//    public function index(NoteRepository $noteRepository): Response
//    {
//        return $this->render('note/index.html.twig', [
//            'notes' => $noteRepository->findAll(),
//        ]);
//    }

//    /**
//     * @Route("/new", name="note_new", methods={"GET","POST"})
//     */
//    public function new(Request $request): Response
//    {
//        $note = new Note();
//        $form = $this->createForm(NoteType::class, $note);
//        $form->handleRequest($request);
//
//        if ($form->isSubmitted() && $form->isValid()) {
//            $entityManager = $this->getDoctrine()->getManager();
//            $entityManager->persist($note);
//            $entityManager->flush();
//
//            return $this->redirectToRoute('note_index');
//        }
//
//        return $this->render('note/newContractNote.html.twig', [
//            'note' => $note,
//            'form' => $form->createView(),
//        ]);
//    }

//    /**
//     * @Route("/{id}", name="note_show", methods={"GET"})
//     */
//    public function show(Note $note): Response
//    {
//        return $this->render('note/show.html.twig', [
//            'note' => $note,
//        ]);
//    }

//    /**
//     * @Route("/{id}/edit", name="note_edit", methods={"GET","POST"})
//     */
//    public function edit(Request $request, Note $note): Response
//    {
//        $form = $this->createForm(NoteType::class, $note);
//        $form->handleRequest($request);
//
//        if ($form->isSubmitted() && $form->isValid()) {
//            $this->getDoctrine()->getManager()->flush();
//
//            return $this->redirectToRoute('note_index');
//        }
//
//        return $this->render('note/edit.html.twig', [
//            'note' => $note,
//            'form' => $form->createView(),
//        ]);
//    }

//    /**
//     * @Route("/{id}", name="note_delete", methods={"POST"})
//     */
//    public function delete(Request $request, Note $note): Response
//    {
//        if ($this->isCsrfTokenValid('delete' . $note->getId(), $request->request->get('_token'))) {
//            $entityManager = $this->getDoctrine()->getManager();
//            $entityManager->remove($note);
//            $entityManager->flush();
//        }
//
//        return $this->redirectToRoute('note_index');
//    }

    /**
     * @Route("/{id}/newContractNote", name="note_new_contract", methods={"GET","POST"})
     * @IsGranted("ROLE_USER")
     */
    public function newContractNote(Request $request, Contract $contract, FileUploader $fileUploader): Response
    {
        $note = new Note();
        $note->setContract($contract);
        $form = $this->createForm(NoteType::class, $note);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // prepare entityManager
            $entityManager = $this->getDoctrine()->getManager();

            // set user and date
            /** @var User $user */
            $user = $this->getUser();
            $note->setDate(new \DateTime());
            $note->setUser($user);

            // persist $note
            $entityManager->persist($note);

            // add files
            /** @var UploadedFile[] $uploadedFiles */
            $uploadedFiles = $form['attachments']->getData();

            if ($uploadedFiles) {
                foreach ($uploadedFiles as $uploadedFile) {
                    $file = $fileUploader->upload($uploadedFile);
                    $file->setNote($note);
                    $entityManager->persist($file);
                }
            }

            // flush
            $entityManager->flush();

            return $this->redirectToRoute('contract_show', ['id' => $contract->getId()]);
        }

        return $this->render('note/newContractNote.html.twig', [
            'note' => $note,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/validateContract", name="note_validate_contract", methods={"GET","POST"})
     * @IsGranted("ROLE_DBM")
     */
    public function validateContract(Request $request, Contract $contract, FileUploader $fileUploader): Response
    {
        $note = new Note();
        $note->setContract($contract);
        $form = $this->createForm(NoteType::class, $note);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // prepare entityManager
            $entityManager = $this->getDoctrine()->getManager();

            // set and persist $note
            /** @var User $user */
            $user = $this->getUser();
            $note->setDate(new \DateTime());
            $note->setUser($user);
            $entityManager->persist($note);

            // add files
            /** @var UploadedFile[] $uploadedFiles */
            $uploadedFiles = $form['attachments']->getData();

            if ($uploadedFiles) {
                foreach ($uploadedFiles as $uploadedFile) {
                    $file = $fileUploader->upload($uploadedFile);
                    $file->setNote($note);
                    $entityManager->persist($file);
                }
            }

            // set and persist $state
            $contract->setCurrentState(self::VALIDATED_TYPE);
            $state = new State();
            $state->setType(self::VALIDATED_TYPE);
            $state->setDate(new \DateTime());
            $state->setUser($user);
            $state->setContract($contract);
            $entityManager->persist($state);

            // flush
            $entityManager->flush();

            return $this->redirectToRoute('contract_index_createdAndNotValidated');
        }

        return $this->render('note/validateContract.html.twig', [
            'note' => $note,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/refuseContract", name="note_refuse_contract", methods={"GET","POST"})
     * @IsGranted("ROLE_DBM")
     */
    public function refuseContract(Request $request, Contract $contract, FileUploader $fileUploader): Response
    {
        $note = new Note();
        $note->setContract($contract);
        $form = $this->createForm(NoteType::class, $note);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // prepare entityManager
            $entityManager = $this->getDoctrine()->getManager();

            // set and persist $note
            /** @var User $user */
            $user = $this->getUser();
            $note->setDate(new \DateTime());
            $note->setUser($user);
            $entityManager->persist($note);

            // add files
            /** @var UploadedFile[] $uploadedFiles */
            $uploadedFiles = $form['attachments']->getData();

            if ($uploadedFiles) {
                foreach ($uploadedFiles as $uploadedFile) {
                    $file = $fileUploader->upload($uploadedFile);
                    $file->setNote($note);
                    $entityManager->persist($file);
                }
            }

            // set and persist $state
            $contract->setCurrentState(self::NOT_VALIDATED_TYPE);
            $state = new State();
            $state->setType(self::NOT_VALIDATED_TYPE);
            $state->setDate(new \DateTime());
            $state->setUser($user);
            $state->setContract($contract);
            $entityManager->persist($state);

            // flush
            $entityManager->flush();

            return $this->redirectToRoute('contract_index_createdAndNotValidated');
        }

        return $this->render('note/refuseContract.html.twig', [
            'note' => $note,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/newConventionNote", name="note_new_convention", methods={"GET","POST"})
     * @IsGranted("ROLE_USER")
     */
    public function newConventionNote(Request $request, Convention $convention, FileUploader $fileUploader): Response
    {
        $note = new Note();
        $note->setConvention($convention);
        $form = $this->createForm(NoteType::class, $note);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // prepare entityManager
            $entityManager = $this->getDoctrine()->getManager();

            // set user and date
            /** @var User $user */
            $user = $this->getUser();
            $note->setDate(new \DateTime());
            $note->setUser($user);

            // persist $note
            $entityManager->persist($note);

            // add files
            /** @var UploadedFile[] $uploadedFiles */
            $uploadedFiles = $form['attachments']->getData();

            if ($uploadedFiles) {
                foreach ($uploadedFiles as $uploadedFile) {
                    $file = $fileUploader->upload($uploadedFile);
                    $file->setNote($note);
                    $entityManager->persist($file);
                }
            }

            // flush
            $entityManager->flush();

            return $this->redirectToRoute('convention_show', ['id' => $convention->getId()]);
        }

        return $this->render('note/newConventionNote.html.twig', [
            'note' => $note,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/validateConvention", name="note_validate_convention", methods={"GET","POST"})
     * @IsGranted("ROLE_DC")
     */
    public function validateConvention(Request $request, Convention $convention, FileUploader $fileUploader): Response
    {
        $note = new Note();
        $note->setConvention($convention);
        $form = $this->createForm(NoteType::class, $note);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // prepare entityManager
            $entityManager = $this->getDoctrine()->getManager();

            // set and persist $note
            /** @var User $user */
            $user = $this->getUser();
            $note->setDate(new \DateTime());
            $note->setUser($user);
            $entityManager->persist($note);

            // add files
            /** @var UploadedFile[] $uploadedFiles */
            $uploadedFiles = $form['attachments']->getData();

            if ($uploadedFiles) {
                foreach ($uploadedFiles as $uploadedFile) {
                    $file = $fileUploader->upload($uploadedFile);
                    $file->setNote($note);
                    $entityManager->persist($file);
                }
            }

            // set and persist $state
            $convention->setCurrentState(self::VALIDATED_TYPE);
            $state = new State();
            $state->setType(self::VALIDATED_TYPE);
            $state->setDate(new \DateTime());
            $state->setUser($user);
            $state->setConvention($convention);
            $entityManager->persist($state);

            // flush
            $entityManager->flush();

            return $this->redirectToRoute('convention_index_createdAndNotValidated');
        }

        return $this->render('note/validateConvention.html.twig', [
            'note' => $note,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/refuseConvention", name="note_refuse_convention", methods={"GET","POST"})
     * @IsGranted("ROLE_DC")
     */
    public function refuseConvention(Request $request, Convention $convention, FileUploader $fileUploader): Response
    {
        $note = new Note();
        $note->setConvention($convention);
        $form = $this->createForm(NoteType::class, $note);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // prepare entityManager
            $entityManager = $this->getDoctrine()->getManager();

            // set and persist $note
            /** @var User $user */
            $user = $this->getUser();
            $note->setDate(new \DateTime());
            $note->setUser($user);
            $entityManager->persist($note);

            // add files
            /** @var UploadedFile[] $uploadedFiles */
            $uploadedFiles = $form['attachments']->getData();

            if ($uploadedFiles) {
                foreach ($uploadedFiles as $uploadedFile) {
                    $file = $fileUploader->upload($uploadedFile);
                    $file->setNote($note);
                    $entityManager->persist($file);
                }
            }

            // set and persist $state
            $convention->setCurrentState(self::NOT_VALIDATED_TYPE);
            $state = new State();
            $state->setType(self::NOT_VALIDATED_TYPE);
            $state->setDate(new \DateTime());
            $state->setUser($user);
            $state->setConvention($convention);
            $entityManager->persist($state);

            // flush
            $entityManager->flush();

            return $this->redirectToRoute('convention_index_createdAndNotValidated');
        }

        return $this->render('note/refuseConvention.html.twig', [
            'note' => $note,
            'form' => $form->createView(),
        ]);
    }
}
