<?php

namespace App\Entity;

use App\Repository\ContractRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinTable;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @ORM\Entity(repositoryClass=ContractRepository::class)
 */
class Contract
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $reference;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $marketReference;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $object;

    /**
     * @ORM\Column(type="datetime")
     */
    private $startDate;

    /**
     * @ORM\Column(type="datetime")
     */
    private $endDate;

    /**
     * @ORM\Column(type="float")
     */
    private $duration;

    /**
     * @ORM\Column(type="float")
     */
    private $amount;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $formalNotice;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $paymentType;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $currentState;

    /**
     * @ORM\Column(type="boolean")
     */
    private $renewable;

    /**
     * @ORM\ManyToOne(targetEntity=Provider::class, inversedBy="contracts")
     * @ORM\JoinColumn(nullable=false)
     */
    private $provider;

    /**
     * @ORM\OneToMany(targetEntity=Note::class, mappedBy="contract")
     */
    private $notes;

    /**
     * @ORM\OneToMany(targetEntity=State::class, mappedBy="contract")
     */
    private $states;

    /**
     * @ORM\ManyToMany(targetEntity=Department::class, inversedBy="availableContractsToAddReviwes")
     * @JoinTable(name="contracts_departments")
     */
    private $availableDepartmentsToAddReviews;

    /**
     * @ORM\OneToMany(targetEntity=File::class, mappedBy="contract")
     */
    private $files;

    private $attachments;

    public function __construct()
    {
        $this->notes = new ArrayCollection();
        $this->states = new ArrayCollection();
        $this->availableDepartmentsToAddReviews = new ArrayCollection();
        $this->files = new ArrayCollection();
        $this->attachments = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    public function getMarketReference(): ?string
    {
        return $this->marketReference;
    }

    public function setMarketReference(string $marketReference): self
    {
        $this->marketReference = $marketReference;

        return $this;
    }

    public function getObject(): ?string
    {
        return $this->object;
    }

    public function setObject(?string $object): self
    {
        $this->object = $object;

        return $this;
    }

    public function getStartDate(): ?\DateTimeInterface
    {
        return $this->startDate;
    }

    public function setStartDate(\DateTimeInterface $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getEndDate(): ?\DateTimeInterface
    {
        return $this->endDate;
    }

    public function setEndDate(\DateTimeInterface $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function getDuration(): ?float
    {
        return $this->duration;
    }

    public function setDuration(float $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    public function getAmount(): ?float
    {
        return $this->amount;
    }

    public function setAmount(float $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getFormalNotice(): ?string
    {
        return $this->formalNotice;
    }

    public function setFormalNotice(string $formalNotice): self
    {
        $this->formalNotice = $formalNotice;

        return $this;
    }

    public function getPaymentType(): ?string
    {
        return $this->paymentType;
    }

    public function setPaymentType(?string $paymentType): self
    {
        $this->paymentType = $paymentType;

        return $this;
    }

    public function getCurrentState(): ?string
    {
        return $this->currentState;
    }

    public function setCurrentState(string $currentState): self
    {
        $this->currentState = $currentState;

        return $this;
    }

    public function getRenewable(): ?bool
    {
        return $this->renewable;
    }

    public function setRenewable(bool $renewable): self
    {
        $this->renewable = $renewable;

        return $this;
    }

    public function getProvider(): ?Provider
    {
        return $this->provider;
    }

    public function setProvider(?Provider $provider): self
    {
        $this->provider = $provider;

        return $this;
    }

    /**
     * @return Collection|Note[]
     */
    public function getNotes(): Collection
    {
        return $this->notes;
    }

    public function addNote(Note $note): self
    {
        if (!$this->notes->contains($note)) {
            $this->notes[] = $note;
            $note->setContract($this);
        }

        return $this;
    }

    public function removeNote(Note $note): self
    {
        if ($this->notes->removeElement($note)) {
            // set the owning side to null (unless already changed)
            if ($note->getContract() === $this) {
                $note->setContract(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|State[]
     */
    public function getStates(): Collection
    {
        return $this->states;
    }

    public function addState(State $state): self
    {
        if (!$this->states->contains($state)) {
            $this->states[] = $state;
            $state->setContract($this);
        }

        return $this;
    }

    public function removeState(State $state): self
    {
        if ($this->states->removeElement($state)) {
            // set the owning side to null (unless already changed)
            if ($state->getContract() === $this) {
                $state->setContract(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Department[]
     */
    public function getAvailableDepartmentsToAddReviews(): Collection
    {
        return $this->availableDepartmentsToAddReviews;
    }

    public function addAvailableDepartmentsToAddReview(Department $availableDepartmentsToAddReview): self
    {
        if (!$this->availableDepartmentsToAddReviews->contains($availableDepartmentsToAddReview)) {
            $this->availableDepartmentsToAddReviews[] = $availableDepartmentsToAddReview;
        }

        return $this;
    }

    public function removeAvailableDepartmentsToAddReview(Department $availableDepartmentsToAddReview): self
    {
        $this->availableDepartmentsToAddReviews->removeElement($availableDepartmentsToAddReview);

        return $this;
    }

    /**
     * @return Collection|File[]
     */
    public function getFiles(): Collection
    {
        return $this->files;
    }

    public function addFile(File $file): self
    {
        if (!$this->files->contains($file)) {
            $this->files[] = $file;
            $file->setContract($this);
        }

        return $this;
    }

    public function removeFile(File $file): self
    {
        if ($this->files->removeElement($file)) {
            // set the owning side to null (unless already changed)
            if ($file->getContract() === $this) {
                $file->setContract(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UploadedFile[]
     */
    public function getAttachments(): Collection
    {
        return $this->attachments;
    }

    public function addAttachment(UploadedFile $file): self
    {
        if (!$this->attachments->contains($file)) {
            $this->attachments[] = $file;
        }

        return $this;
    }

    public function removeAttachment(UploadedFile $file): self
    {
        $this->attachments->removeElement($file);
        return $this;
    }
}
