<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\Table(name="`user`")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $phoneNumber;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isBlocked;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Department", inversedBy="users")
     * @ORM\JoinColumn(name="department_id", referencedColumnName="id", nullable=true)
     */
    private $department;

    /**
     * @ORM\OneToMany(targetEntity=Note::class, mappedBy="user")
     */
    private $contractsNotesAdded;

    /**
     * @ORM\OneToMany(targetEntity=State::class, mappedBy="user")
     */
    private $contractsStatesAdded;

    public function __construct()
    {
        $this->contractsNotesAdded = new ArrayCollection();
        $this->contractsStatesAdded = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string)$this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
//        $roles = $this->roles;
//        $roles[] = 'ROLE_USER';
//        return array_unique($roles);

        return $this->roles;
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function getIsBlocked(): ?bool
    {
        return $this->isBlocked;
    }

    public function setIsBlocked(bool $isBlocked): self
    {
        $this->isBlocked = $isBlocked;

        return $this;
    }

    public function getDepartment(): ?Department
    {
        return $this->department;
    }

    public function setDepartment(?Department $department): self
    {
        $this->department = $department;

        return $this;
    }

    /**
     * @return Collection|Note[]
     */
    public function getContractsNotesAdded(): Collection
    {
        return $this->contractsNotesAdded;
    }

    public function addContractNote(Note $note): self
    {
        if (!$this->contractsNotesAdded->contains($note)) {
            $this->contractsNotesAdded[] = $note;
            $note->setUser($this);
        }

        return $this;
    }

    public function removeContractNote(Note $note): self
    {
        if ($this->contractsNotesAdded->removeElement($note)) {
            // set the owning side to null (unless already changed)
            if ($note->getUser() === $this) {
                $note->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|State[]
     */
    public function getContractsStatesAdded(): Collection
    {
        return $this->contractsStatesAdded;
    }

    public function addContractState(State $contractStateAdded): self
    {
        if (!$this->contractsStatesAdded->contains($contractStateAdded)) {
            $this->contractsStatesAdded[] = $contractStateAdded;
            $contractStateAdded->setUser($this);
        }

        return $this;
    }

    public function removeContractState(State $contractStateAdded): self
    {
        if ($this->contractsStatesAdded->removeElement($contractStateAdded)) {
            // set the owning side to null (unless already changed)
            if ($contractStateAdded->getUser() === $this) {
                $contractStateAdded->setUser(null);
            }
        }

        return $this;
    }

    public function addContractsNotesAdded(Note $contractsNotesAdded): self
    {
        if (!$this->contractsNotesAdded->contains($contractsNotesAdded)) {
            $this->contractsNotesAdded[] = $contractsNotesAdded;
            $contractsNotesAdded->setUser($this);
        }

        return $this;
    }

    public function removeContractsNotesAdded(Note $contractsNotesAdded): self
    {
        if ($this->contractsNotesAdded->removeElement($contractsNotesAdded)) {
            // set the owning side to null (unless already changed)
            if ($contractsNotesAdded->getUser() === $this) {
                $contractsNotesAdded->setUser(null);
            }
        }

        return $this;
    }

    public function addContractsStatesAdded(State $contractsStatesAdded): self
    {
        if (!$this->contractsStatesAdded->contains($contractsStatesAdded)) {
            $this->contractsStatesAdded[] = $contractsStatesAdded;
            $contractsStatesAdded->setUser($this);
        }

        return $this;
    }

    public function removeContractsStatesAdded(State $contractsStatesAdded): self
    {
        if ($this->contractsStatesAdded->removeElement($contractsStatesAdded)) {
            // set the owning side to null (unless already changed)
            if ($contractsStatesAdded->getUser() === $this) {
                $contractsStatesAdded->setUser(null);
            }
        }

        return $this;
    }
}
