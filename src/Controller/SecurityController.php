<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserEmailType;
use App\Repository\UserRepository;
use App\Service\Mailer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{

    /**
     * @Route("/", name="app_home")
     */
    public function home(): Response
    {
        /** @var User $user */
        $user = $this->getUser();

        if (!$user) {
            return $this->redirectToRoute('app_login');
        } elseif (in_array('ROLE_USER', $user->getRoles())) {
            return $this->redirectToRoute('contract_index_created');
        } elseif (in_array('ROLE_DBM', $user->getRoles())) {
            return $this->redirectToRoute('contract_index_createdAndNotValidated');
        } elseif (in_array('ROLE_DC', $user->getRoles())) {
            return $this->redirectToRoute('convention_index_createdAndNotValidated');
        } elseif (in_array('ROLE_DF', $user->getRoles())) {
            return $this->redirectToRoute('contract_index_validatedAndPayedAndNotPayed');
        } else {
            return $this->redirectToRoute('user_index');
        }
    }


    /**
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // if ($this->getUser()) {
        //     return $this->redirectToRoute('target_path');
        // }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }

    /**
     * @Route("/newpassword", name="app_new_password", methods={"GET","POST"})
     */
    public function newPassword(
        Request $request,
        UserPasswordEncoderInterface $passwordEncoder,
        UserRepository $userRepository,
        Mailer $mailer
    ): Response
    {
        $user = new User();
        $form = $this->createForm(UserEmailType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // check user if exist
            $fetchedUser = $userRepository->findOneBy(['email' => $user->getEmail()]);

            if (!$fetchedUser) {
                $this->addFlash(
                    'error',
                    'l\'adresse e-mail est introuvable.'
                );
                return $this->redirectToRoute('app_login');
            }

            $newPassword = $mailer->sendNewPasswordMail(
                $fetchedUser->getEmail(),
                $fetchedUser->getFirstName() . ' ' . $fetchedUser->getLastName()
            );

            if (!$newPassword) {
                $this->addFlash(
                    'error',
                    'Le système ne peut pas envoyer de nouveau mot de passe pour le moment.'
                );
                return $this->redirectToRoute('app_login');
            }

            $fetchedUser->setPassword($passwordEncoder->encodePassword(
                $fetchedUser,
                $newPassword
            ));

            $this->addFlash(
                'success',
                'Un nouveau mot de passe a été envoyé à ' . $fetchedUser->getEmail() . '.'
            );

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($fetchedUser);
            $entityManager->flush();

            return $this->redirectToRoute('app_login');
        }

        return $this->render('security/newPassword.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }
}
