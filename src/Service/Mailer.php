<?php

namespace App\Service;

use App\Entity\User;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class Mailer
{

    private MailerInterface $mailerInterface;

    /**
     * Mailer constructor.
     * @param MailerInterface $mailerInterface
     */
    public function __construct(MailerInterface $mailerInterface)
    {
        $this->mailerInterface = $mailerInterface;
    }

    public function sendNewAccountMail(User $user): bool
    {
        $email = (new Email())
            ->from('cnicontrat@gmail.com')
            ->to($user->getEmail())
            ->subject('Nouveau compte!')
            ->html('<p>Bonjour ' . $user->getFirstName() . ' ' . $user->getLastName() . ',</p>
                          <p>Un nouveau compte a été ajouté dans notre plateforme CNI.</p>
                          <p>Votre mot de passe est: ' . $user->getPassword() . '</p>
                          <p>Rejoignez-nous ici: <a href="http://127.0.0.1:8000/login">cni</a></p>');

        try {
            $this->mailerInterface->send($email);
            return true;
        } catch (TransportExceptionInterface $e) {
            return false;
        }
    }

    public function sendNewPasswordMail(string $userEmail, string $userFullName): ?string
    {
        $newPassword = $this->getNewPassword();
        $email = (new Email())
            ->from('cnicontrat@gmail.com')
            ->to($userEmail)
            ->subject('Nouveau mot de passe!')
            ->html('<p>Bonjour ' . $userFullName . ',</p>
                          <p>Votre nouveau mot de passe est: ' . $newPassword . '</p>');

        try {
            $this->mailerInterface->send($email);
            return $newPassword;
        } catch (TransportExceptionInterface $e) {
            return null;
        }
    }

    private function getNewPassword(): string
    {
        $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $input_length = strlen($permitted_chars);
        $random_string = '';
        for ($i = 0; $i < 10; $i++) {
            $random_character = $permitted_chars[mt_rand(0, $input_length - 1)];
            $random_string .= $random_character;
        }
        return $random_string;
    }
}
