<?php

namespace App\Entity;

use App\Repository\ConventionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinTable;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @ORM\Entity(repositoryClass=ConventionRepository::class)
 */
class Convention
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $projectTitle;

    /**
     * @ORM\OneToMany(targetEntity=Note::class, mappedBy="convention")
     */
    private $notes;

    /**
     * @ORM\OneToMany(targetEntity=State::class, mappedBy="convention")
     */
    private $states;

    /**
     * @ORM\OneToMany(targetEntity=File::class, mappedBy="convention")
     */
    private $files;

    /**
     * @ORM\Column(type="float")
     */
    private $estimatedAmount;

    /**
     * @ORM\Column(type="float")
     */
    private $invoiceAmount;

    /**
     * @ORM\Column(type="datetime")
     */
    private $signatureDate;

    /**
     * @ORM\Column(type="datetime")
     */
    private $workStartDate;

    /**
     * @ORM\Column(type="datetime")
     */
    private $workEndDate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $paymentType;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $currentState;

    /**
     * @ORM\Column(type="boolean")
     */
    private $renewable;

    /**
     * @ORM\ManyToOne(targetEntity=Client::class, inversedBy="conventions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $client;

    /**
     * @ORM\ManyToMany(targetEntity=Department::class, inversedBy="availableConventionsToAddReviewes")
     * @JoinTable(name="conventions_departments")
     */
    private $availableDepartmentsToAddReviews;

    private $attachments;

    public function __construct()
    {
        $this->notes = new ArrayCollection();
        $this->states = new ArrayCollection();
        $this->files = new ArrayCollection();
        $this->availableDepartmentsToAddReviews = new ArrayCollection();
        $this->attachments = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProjectTitle(): ?string
    {
        return $this->projectTitle;
    }

    public function setProjectTitle(string $projectTitle): self
    {
        $this->projectTitle = $projectTitle;

        return $this;
    }

    /**
     * @return Collection|Note[]
     */
    public function getNotes(): Collection
    {
        return $this->notes;
    }

    public function addNote(Note $note): self
    {
        if (!$this->notes->contains($note)) {
            $this->notes[] = $note;
            $note->setConvention($this);
        }

        return $this;
    }

    public function removeNote(Note $note): self
    {
        if ($this->notes->removeElement($note)) {
            // set the owning side to null (unless already changed)
            if ($note->getConvention() === $this) {
                $note->setConvention(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|State[]
     */
    public function getStates(): Collection
    {
        return $this->states;
    }

    public function addState(State $state): self
    {
        if (!$this->states->contains($state)) {
            $this->states[] = $state;
            $state->setConvention($this);
        }

        return $this;
    }

    public function removeState(State $state): self
    {
        if ($this->states->removeElement($state)) {
            // set the owning side to null (unless already changed)
            if ($state->getConvention() === $this) {
                $state->setConvention(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|File[]
     */
    public function getFiles(): Collection
    {
        return $this->files;
    }

    public function addFile(File $file): self
    {
        if (!$this->files->contains($file)) {
            $this->files[] = $file;
            $file->setConvention($this);
        }

        return $this;
    }

    public function removeFile(File $file): self
    {
        if ($this->files->removeElement($file)) {
            // set the owning side to null (unless already changed)
            if ($file->getConvention() === $this) {
                $file->setConvention(null);
            }
        }

        return $this;
    }

    public function getEstimatedAmount(): ?float
    {
        return $this->estimatedAmount;
    }

    public function setEstimatedAmount(float $estimatedAmount): self
    {
        $this->estimatedAmount = $estimatedAmount;

        return $this;
    }

    public function getInvoiceAmount(): ?float
    {
        return $this->invoiceAmount;
    }

    public function setInvoiceAmount(float $invoiceAmount): self
    {
        $this->invoiceAmount = $invoiceAmount;

        return $this;
    }

    public function getSignatureDate(): ?\DateTimeInterface
    {
        return $this->signatureDate;
    }

    public function setSignatureDate(\DateTimeInterface $signatureDate): self
    {
        $this->signatureDate = $signatureDate;

        return $this;
    }

    public function getWorkStartDate(): ?\DateTimeInterface
    {
        return $this->workStartDate;
    }

    public function setWorkStartDate(\DateTimeInterface $workStartDate): self
    {
        $this->workStartDate = $workStartDate;

        return $this;
    }

    public function getWorkEndDate(): ?\DateTimeInterface
    {
        return $this->workEndDate;
    }

    public function setWorkEndDate(\DateTimeInterface $workEndDate): self
    {
        $this->workEndDate = $workEndDate;

        return $this;
    }

    public function getPaymentType(): ?string
    {
        return $this->paymentType;
    }

    public function setPaymentType(?string $paymentType): self
    {
        $this->paymentType = $paymentType;

        return $this;
    }

    public function getCurrentState(): ?string
    {
        return $this->currentState;
    }

    public function setCurrentState(string $currentState): self
    {
        $this->currentState = $currentState;

        return $this;
    }

    public function getRenewable(): ?bool
    {
        return $this->renewable;
    }

    public function setRenewable(bool $renewable): self
    {
        $this->renewable = $renewable;

        return $this;
    }

    public function getClient(): ?Client
    {
        return $this->client;
    }

    public function setClient(?Client $client): self
    {
        $this->client = $client;

        return $this;
    }

    /**
     * @return Collection|Department[]
     */
    public function getAvailableDepartmentsToAddReviews(): Collection
    {
        return $this->availableDepartmentsToAddReviews;
    }

    public function addAvailableDepartmentsToAddReview(Department $availableDepartmentsToAddReview): self
    {
        if (!$this->availableDepartmentsToAddReviews->contains($availableDepartmentsToAddReview)) {
            $this->availableDepartmentsToAddReviews[] = $availableDepartmentsToAddReview;
        }

        return $this;
    }

    public function removeAvailableDepartmentsToAddReview(Department $availableDepartmentsToAddReview): self
    {
        $this->availableDepartmentsToAddReviews->removeElement($availableDepartmentsToAddReview);

        return $this;
    }

    /**
     * @return Collection|UploadedFile[]
     */
    public function getAttachments(): Collection
    {
        return $this->attachments;
    }

    public function addAttachment(UploadedFile $file): self
    {
        if (!$this->attachments->contains($file)) {
            $this->attachments[] = $file;
        }

        return $this;
    }

    public function removeAttachment(UploadedFile $file): self
    {
        $this->attachments->removeElement($file);
        return $this;
    }
}
