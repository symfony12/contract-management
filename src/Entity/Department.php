<?php

namespace App\Entity;

use App\Repository\DepartmentRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DepartmentRepository::class)
 */
class Department
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $code;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\User", mappedBy="department")
     */
    private $users;

    /**
     * @ORM\ManyToMany(targetEntity=Contract::class, mappedBy="availableDepartmentsToAddReviews")
     */
    private $availableContractsToAddReviwes;

    /**
     * @ORM\ManyToMany(targetEntity=Convention::class, mappedBy="availableDepartmentsToAddReviews")
     */
    private $availableConventionsToAddReviewes;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->availableContractsToAddReviwes = new ArrayCollection();
        $this->availableConventionsToAddReviewes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->setDepartment($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->removeElement($user)) {
            // set the owning side to null (unless already changed)
            if ($user->getDepartment() === $this) {
                $user->setDepartment(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Contract[]
     */
    public function getAvailableContractsToAddReviwes(): Collection
    {
        return $this->availableContractsToAddReviwes;
    }

    public function addAvailableContractsToAddReviwe(Contract $availableContractsToAddReviwe): self
    {
        if (!$this->availableContractsToAddReviwes->contains($availableContractsToAddReviwe)) {
            $this->availableContractsToAddReviwes[] = $availableContractsToAddReviwe;
            $availableContractsToAddReviwe->addAvailableDepartmentsToAddReview($this);
        }

        return $this;
    }

    public function removeAvailableContractsToAddReviwe(Contract $availableContractsToAddReviwe): self
    {
        if ($this->availableContractsToAddReviwes->removeElement($availableContractsToAddReviwe)) {
            $availableContractsToAddReviwe->removeAvailableDepartmentsToAddReview($this);
        }

        return $this;
    }

    /**
     * @return Collection|Convention[]
     */
    public function getAvailableConventionsToAddReviewes(): Collection
    {
        return $this->availableConventionsToAddReviewes;
    }

    public function addAvailableConventionsToAddReviewe(Convention $availableConventionsToAddReviewe): self
    {
        if (!$this->availableConventionsToAddReviewes->contains($availableConventionsToAddReviewe)) {
            $this->availableConventionsToAddReviewes[] = $availableConventionsToAddReviewe;
            $availableConventionsToAddReviewe->addAvailableDepartmentsToAddReview($this);
        }

        return $this;
    }

    public function removeAvailableConventionsToAddReviewe(Convention $availableConventionsToAddReviewe): self
    {
        if ($this->availableConventionsToAddReviewes->removeElement($availableConventionsToAddReviewe)) {
            $availableConventionsToAddReviewe->removeAvailableDepartmentsToAddReview($this);
        }

        return $this;
    }
}
