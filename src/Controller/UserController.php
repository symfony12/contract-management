<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserEditType;
use App\Form\UserType;
use App\Repository\UserRepository;
use App\Service\Mailer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/user")
 * @IsGranted("ROLE_ADMIN")
 */
class UserController extends AbstractController
{
    /**
     * @Route("/", name="user_index", methods={"GET"})
     */
    public function index(UserRepository $userRepository): Response
    {
        return $this->render('user/index.html.twig', [
            'users' => $userRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="user_new", methods={"GET","POST"})
     */
    public function new(Request $request, UserPasswordEncoderInterface $passwordEncoder, Mailer $mailer): Response
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $newAccountMailIsSent = $mailer->sendNewAccountMail($user);
            if (!$newAccountMailIsSent) {
                $this->addFlash(
                    'error',
                    'Le système ne peut pas envoyer le mot de passe pour le moment.L\'ajout de l\'utilisateur est annulé.'
                );
                return $this->redirectToRoute('user_new');
            }

            $user->setIsBlocked(false);
            $passwordNotEncrypted = $user->getPassword();
            $user->setPassword($passwordEncoder->encodePassword(
                $user,
                $passwordNotEncrypted
            ));

            if ($user->getDepartment() === null) {
                $user->setRoles(['ROLE_ADMIN']);
            } elseif ($user->getDepartment()->getId() === 1) {
                $user->setRoles(['ROLE_DBM']);
            } elseif ($user->getDepartment()->getId() === 2) {
                $user->setRoles(['ROLE_DC']);
            } elseif ($user->getDepartment()->getId() === 3) {
                $user->setRoles(['ROLE_DF']);
            } else {
                $user->setRoles(['ROLE_USER']);
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('user_index');
        }

        return $this->render('user/new.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="user_show", methods={"GET"})
     */
    public function show(User $user): Response
    {
        return $this->render('user/show.html.twig', [
            'user' => $user,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="user_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, User $user): Response
    {
        $form = $this->createForm(UserEditType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('user_index');
        }

        return $this->render('user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="user_delete", methods={"POST"})
     */
    public function delete(Request $request, User $user): Response
    {
        if ($this->isCsrfTokenValid('delete' . $user->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($user);
            $entityManager->flush();
        }

        return $this->redirectToRoute('user_index');
    }

    /**
     * @Route("/{id}/block", name="user_block", methods={"GET"})
     */
    public function block(User $user): Response
    {
        $user->setIsBlocked(!$user->getIsBlocked());
        $this->getDoctrine()->getManager()->flush();
        return $this->redirectToRoute('user_index');
    }
}
