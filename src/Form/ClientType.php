<?php

namespace App\Form;

use App\Entity\Client;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;

class ClientType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Nom',
                'required' => true
            ])
            ->add('email', EmailType::class, [
                'label' => 'Email',
                'required' => true
            ])
            ->add('phoneNumber', TelType::class, [
                'label' => 'Numéro de téléphone',
                'required' => false,
                'constraints' => [new Length([
                    'min' => 8,
                    'max' => 15,
                    'minMessage' => 'Cette valeur est trop courte. Il doit contenir au moins 8 caractères.',
                    'maxMessage' => 'Cette valeur est trop longue. Il doit contenir 15 caractères ou moins.'
                ])]
            ])
            ->add('faxNumber', TelType::class, [
                'label' => 'Numéro de fax',
                'required' => false,
                'constraints' => [new Length([
                    'min' => 8,
                    'max' => 15,
                    'minMessage' => 'Cette valeur est trop courte. Il doit contenir au moins 8 caractères.',
                    'maxMessage' => 'Cette valeur est trop longue. Il doit contenir 15 caractères ou moins.'
                ])]
            ])
            ->add('address', TextType::class, [
                'label' => 'Adresse',
                'required' => false
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Client::class,
        ]);
    }
}
