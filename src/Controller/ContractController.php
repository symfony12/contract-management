<?php

namespace App\Controller;

use App\Entity\Contract;
use App\Entity\State;
use App\Entity\User;
use App\Form\ContractEditType;
use App\Form\ContractPaymentType;
use App\Form\ContractType;
use App\Repository\ContractRepository;
use App\Service\FileUploader;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/contract")
 */
class ContractController extends AbstractController
{
    const CREATED_TYPE = 'CREATED';
    const VALIDATED_TYPE = 'VALIDATED';
    const NOT_VALIDATED_TYPE = 'NOT_VALIDATED';
    const PAYED_TYPE = 'PAYED';
    const NOT_PAYED_TYPE = 'NOT_PAYED';

    /**
     * @Route("/", name="contract_index", methods={"GET"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function index(ContractRepository $contractRepository): Response
    {
        return $this->render('contract/index.html.twig', [
            'contracts' => $contractRepository->findAll(),
        ]);
    }

    /**
     * @Route("/created", name="contract_index_created", methods={"GET"})
     * @IsGranted("ROLE_USER")
     */
    public function getCreatedContracts(ContractRepository $contractRepository): Response
    {
        /** @var User $user */
        $user = $this->getUser();

        $contracts = [];
        $createdContractsToReview = [];
        $createdContractsReviewed = [];
        $allContractsReviewed = [];

        $allCreatedContracts = $contractRepository->findBy(
            ['currentState' => self::CREATED_TYPE]
        );
        $allContracts = $contractRepository->findAll();

        foreach ($allContracts as $contract) {
            $test = false;
            if (($contract->getNotes()) && (count($contract->getNotes()) > 0)) {
                foreach ($contract->getNotes() as $note) {
                    if ($note->getUser()->getDepartment() === $user->getDepartment()) {
                        $test = true;
                        break;
                    }
                }
            }
            if ($test) {
                array_push($allContractsReviewed, $contract);
            }
        }

        foreach ($allCreatedContracts as $contract) {
            if ($contract->getAvailableDepartmentsToAddReviews()->contains($user->getDepartment())) {
                array_push($contracts, $contract);
            }
        }

        foreach ($contracts as $contract) {
            $test = false;

            if (($contract->getNotes()) && (count($contract->getNotes()) > 0)) {
                foreach ($contract->getNotes() as $note) {
                    if ($note->getUser()->getDepartment() === $user->getDepartment()) {
                        $test = true;
                        break;
                    }
                }
            }

            if ($test) {
                array_push($createdContractsReviewed, $contract);
            } else {
                array_push($createdContractsToReview, $contract);
            }
        }

        return $this->render('contract/createdContracts.html.twig', [
            'contracts' => $createdContractsToReview,
            'contractsReviewed' => $createdContractsReviewed,
            'allContractsReviewed' => $allContractsReviewed
        ]);
    }

    /**
     * @Route("/createdAndNotValidated", name="contract_index_createdAndNotValidated", methods={"GET"})
     * @IsGranted("ROLE_DBM")
     */
    public function getCreatedAndNotValidatedContracts(ContractRepository $contractRepository): Response
    {
        return $this->render('contract/createdAndNotValidatedContracts.html.twig', [
            'createdContracts' => $contractRepository->findBy(['currentState' => self::CREATED_TYPE]),
            'validatedContracts' => $contractRepository->findBy(['currentState' => self::VALIDATED_TYPE]),
            'notValidatedContracts' => $contractRepository->findBy(['currentState' => self::NOT_VALIDATED_TYPE])
        ]);
    }

    /**
     * @Route("/validatedAndPayedAndNotPayed", name="contract_index_validatedAndPayedAndNotPayed", methods={"GET"})
     * @IsGranted("ROLE_DF")
     */
    public function getValidatedAndPayedAndNotPayedContracts(ContractRepository $contractRepository): Response
    {
        return $this->render('contract/validatedAndPayedAndNotPayedContracts.html.twig', [
            'validatedContracts' => $contractRepository->findBy(['currentState' => self::VALIDATED_TYPE]),
            'payedContracts' => $contractRepository->findBy(['currentState' => self::PAYED_TYPE]),
            'notPayedContracts' => $contractRepository->findBy(['currentState' => self::NOT_PAYED_TYPE])
        ]);
    }

    /**
     * @Route("/new", name="contract_new", methods={"GET","POST"})
     * @IsGranted("ROLE_DBM")
     */
    public function new(Request $request, FileUploader $fileUploader): Response
    {
        $newDate = new \DateTime();
        $endDate = $newDate->add(new \DateInterval('P10D'));
        $endDate->setTime(0, 0);

        $contract = new Contract();
        $contract->setStartDate(new \DateTime());
        $contract->setEndDate($endDate);

        $form = $this->createForm(ContractType::class, $contract);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //check dates
            if (!($contract->getEndDate() > $contract->getStartDate())) {
                $this->addFlash(
                    'error',
                    'La date de fin de contrat doit être postérieure à la date d\'entrée en vigueur !'
                );
                return $this->redirectToRoute('contract_new');
            }

            // prepare entityManager
            $entityManager = $this->getDoctrine()->getManager();

            // set and persist $contract
            $startDate = $contract->getStartDate();
            $endDate = $contract->getEndDate();
            $diff = $startDate->diff($endDate);
            $contract->setDuration($diff->d);
            $contract->setCurrentState(self::CREATED_TYPE);
            $entityManager->persist($contract);

            // add files
            /** @var UploadedFile[] $uploadedFiles */
            $uploadedFiles = $form['attachments']->getData();

            if ($uploadedFiles) {
                foreach ($uploadedFiles as $uploadedFile) {
                    $file = $fileUploader->upload($uploadedFile);
                    $file->setContract($contract);
                    $entityManager->persist($file);
                }
            }

            // set and persist $state
            /** @var User $user */
            $user = $this->getUser();

            $state = new State();
            $state->setType(self::CREATED_TYPE);
            $state->setDate(new \DateTime());
            $state->setUser($user);
            $state->setContract($contract);
            $entityManager->persist($state);

            // flush
            $entityManager->flush();

            return $this->redirectToRoute('contract_index_createdAndNotValidated');
        }

        return $this->render('contract/new.html.twig', [
            'contract' => $contract,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="contract_show", methods={"GET"})
     * @Security("is_granted('ROLE_USER') or is_granted('ROLE_DBM') or is_granted('ROLE_DF') or is_granted('ROLE_ADMIN')")
     */
    public function show(Contract $contract): Response
    {
        /** @var User $user */
        $user = $this->getUser();

        $canReviewed = (in_array('ROLE_USER', $user->getRoles())) &&
            ($contract->getCurrentState() === self::CREATED_TYPE) &&
            (!$contract->getAvailableDepartmentsToAddReviews()->isEmpty()) &&
            ($contract->getAvailableDepartmentsToAddReviews()->contains($user->getDepartment()));

        $canValidated = (in_array('ROLE_DBM', $user->getRoles())) &&
            ($contract->getCurrentState() === self::CREATED_TYPE) &&
            (
                ($contract->getAvailableDepartmentsToAddReviews()->isEmpty()) ||
                ((!$contract->getAvailableDepartmentsToAddReviews()->isEmpty()) && (!$contract->getNotes()->isEmpty()))
            );

        $canPayed = (in_array('ROLE_DF', $user->getRoles())) && ($contract->getCurrentState() === self::VALIDATED_TYPE);

        return $this->render('contract/show.html.twig', [
            'contract' => $contract,
            'canReviewed' => $canReviewed,
            'canValidated' => $canValidated,
            'canPayed' => $canPayed
        ]);
    }

    /**
     * @Route("/{id}/edit", name="contract_edit", methods={"GET","POST"})
     * @IsGranted("ROLE_DBM")
     */
    public function edit(Request $request, Contract $contract): Response
    {
        $form = $this->createForm(ContractEditType::class, $contract);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //check dates
            if (!($contract->getEndDate() > $contract->getStartDate())) {
                $this->addFlash(
                    'error',
                    'La date de fin de contrat doit être postérieure à la date d\'entrée en vigueur !'
                );
                return $this->redirectToRoute('contract_edit', ['id' => $contract->getId()]);
            }

            // set $contract
            $startDate = $contract->getStartDate();
            $endDate = $contract->getEndDate();
            $diff = $startDate->diff($endDate);
            $contract->setDuration($diff->d);

            // save
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($contract);
            $entityManager->flush();

            return $this->redirectToRoute('contract_show', ['id' => $contract->getId()]);
        }

        return $this->render('contract/edit.html.twig', [
            'contract' => $contract,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="contract_delete", methods={"POST"})
     * @IsGranted("ROLE_DBM")
     */
    public function delete(Request $request, Contract $contract): Response
    {
        if ($this->isCsrfTokenValid('delete' . $contract->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();

            foreach ($contract->getStates() as $state) {
                $entityManager->remove($state);
            }

            foreach ($contract->getFiles() as $file) {
                $entityManager->remove($file);
            }

            $entityManager->remove($contract);
            $entityManager->flush();
        }

        return $this->redirectToRoute('contract_index_createdAndNotValidated');
    }

    /**
     * @Route("/{id}/updatePayed", name="contract_edit_status_payed", methods={"GET","POST"})
     * @IsGranted("ROLE_DF")
     */
    public function updateContractStatusPayed(Request $request, Contract $contract): Response
    {
        $form = $this->createForm(ContractPaymentType::class, $contract);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $contract->setCurrentState(self::PAYED_TYPE);

            /** @var User $user */
            $user = $this->getUser();

            $state = new State();
            $state->setType(self::PAYED_TYPE);
            $state->setDate(new \DateTime());
            $state->setUser($user);
            $state->setContract($contract);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($contract);
            $entityManager->persist($state);
            $entityManager->flush();

            return $this->redirectToRoute('contract_index_validatedAndPayedAndNotPayed');
        }

        return $this->render('contract/payed.html.twig', [
            'contract' => $contract,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/updateNotPayed", name="contract_edit_status_not_payed", methods={"GET"})
     * @IsGranted("ROLE_DF")
     */
    public function updateContractStatusNotPayed(Contract $contract): Response
    {
        $contract->setCurrentState(self::NOT_PAYED_TYPE);

        /** @var User $user */
        $user = $this->getUser();

        $state = new State();
        $state->setType(self::NOT_PAYED_TYPE);
        $state->setDate(new \DateTime());
        $state->setUser($user);
        $state->setContract($contract);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($contract);
        $entityManager->persist($state);
        $entityManager->flush();

        return $this->redirectToRoute('contract_index_validatedAndPayedAndNotPayed');
    }
}
