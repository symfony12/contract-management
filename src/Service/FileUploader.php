<?php

namespace App\Service;

use App\Entity\File;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\String\Slugger\SluggerInterface;

class FileUploader
{
    private $targetDirectory;
    private $slugger;

    public function __construct($targetDirectory, SluggerInterface $slugger)
    {
        $this->targetDirectory = $targetDirectory;
        $this->slugger = $slugger;
    }

    public function upload(UploadedFile $uploadedFile): File
    {
        $originalFilename = pathinfo($uploadedFile->getClientOriginalName(), PATHINFO_FILENAME);
        $safeFilename = $this->slugger->slug($originalFilename);
        $fileName = $safeFilename . '-' . uniqid() . '.' . $uploadedFile->guessExtension();

        $file = new File();
        $file->setName($uploadedFile->getClientOriginalName());
        $file->setType($uploadedFile->guessExtension());
        $file->setExtention($uploadedFile->guessExtension());
        $file->setOldPath($originalFilename);
        $file->setNewPath($fileName);

        try {
            $uploadedFile->move($this->getTargetDirectory(), $fileName);
        } catch (FileException $e) {
            // ... handle exception if something happens during file upload
        }

        return $file;
    }

    public function getTargetDirectory()
    {
        return $this->targetDirectory;
    }
}