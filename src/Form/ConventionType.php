<?php

namespace App\Form;

use App\Entity\Client;
use App\Entity\Convention;
use App\Entity\Department;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ConventionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('projectTitle', TextType::class, [
                'label' => 'Titre du projet',
                'required' => true
            ])
            ->add('estimatedAmount', NumberType::class, [
                'label' => 'Montant estimatif',
                'required' => true
            ])
            ->add('invoiceAmount', NumberType::class, [
                'label' => 'Montant facture',
                'required' => true
            ])
            ->add('signatureDate', DateTimeType::class, [
                'label' => 'Date de signature',
                'required' => true
            ])
            ->add('workStartDate', DateTimeType::class, [
                'label' => 'Début de travaux',
                'required' => true
            ])
            ->add('workEndDate', DateTimeType::class, [
                'label' => 'Fin des travaux',
                'required' => true
            ])
            ->add('renewable', RadioType::class, [
                'label' => 'Renouvelable',
                'required' => false
            ])
            ->add('client', EntityType::class, [
                'label' => 'Client',
                'class' => Client::class,
                'choice_label' => 'name',
                'placeholder' => 'Sélectionnez',
                'required' => true
            ])
            ->add('availableDepartmentsToAddReviews', EntityType::class, [
                'label' => 'Directions',
                'class' => Department::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->where('c.id != ?1')
                        ->andWhere('c.id != ?2')
                        ->andWhere('c.id != ?3')
                        ->setParameter(1, 1)
                        ->setParameter(2, 2)
                        ->setParameter(3, 3);
                },
                'choice_label' => 'name',
                'multiple' => true,
                'expanded' => false,
                'required' => false
            ])
            ->add('attachments', FileType::class, [
                'label'=> 'Pièces jointes',
                'multiple' => true,
                'required' => false
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Convention::class,
        ]);
    }
}
