<?php

namespace App\Controller;

use App\Entity\Convention;
use App\Entity\State;
use App\Entity\User;
use App\Form\ConventionEditType;
use App\Form\ConventionPaymentType;
use App\Form\ConventionType;
use App\Repository\ConventionRepository;
use App\Service\FileUploader;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/convention")
 */
class ConventionController extends AbstractController
{
    const CREATED_TYPE = 'CREATED';
    const VALIDATED_TYPE = 'VALIDATED';
    const NOT_VALIDATED_TYPE = 'NOT_VALIDATED';
    const PAYED_TYPE = 'PAYED';
    const NOT_PAYED_TYPE = 'NOT_PAYED';

    /**
     * @Route("/", name="convention_index", methods={"GET"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function index(ConventionRepository $conventionRepository): Response
    {
        return $this->render('convention/index.html.twig', [
            'conventions' => $conventionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/created", name="convention_index_created", methods={"GET"})
     * @IsGranted("ROLE_USER")
     */
    public function getCreatedConventions(ConventionRepository $conventionRepository): Response
    {
        /** @var User $user */
        $user = $this->getUser();

        $conventions = [];
        $createdConventionsToReview = [];
        $createdConventionsReviewed = [];
        $allConventionsReviewed = [];

        $allCreatedConventions = $conventionRepository->findBy(
            ['currentState' => self::CREATED_TYPE]
        );
        $allConventions = $conventionRepository->findAll();

        foreach ($allConventions as $convention) {
            $test = false;
            if (($convention->getNotes()) && (count($convention->getNotes()) > 0)) {
                foreach ($convention->getNotes() as $note) {
                    if ($note->getUser()->getDepartment() === $user->getDepartment()) {
                        $test = true;
                        break;
                    }
                }
            }
            if ($test) {
                array_push($allConventionsReviewed, $convention);
            }
        }

        foreach ($allCreatedConventions as $contract) {
            if ($contract->getAvailableDepartmentsToAddReviews()->contains($user->getDepartment())) {
                array_push($conventions, $contract);
            }
        }

        foreach ($conventions as $convention) {
            $test = false;

            if (($convention->getNotes()) && (count($convention->getNotes()) > 0)) {
                foreach ($convention->getNotes() as $note) {
                    if ($note->getUser()->getDepartment() === $user->getDepartment()) {
                        $test = true;
                        break;
                    }
                }
            }

            if ($test) {
                array_push($createdConventionsReviewed, $convention);
            } else {
                array_push($createdConventionsToReview, $convention);
            }
        }

        return $this->render('convention/createdConventions.html.twig', [
            'conventions' => $createdConventionsToReview,
            'conventionsReviewed' => $createdConventionsReviewed,
            'allConventionsReviewed' => $allConventionsReviewed
        ]);
    }

    /**
     * @Route("/createdAndNotValidated", name="convention_index_createdAndNotValidated", methods={"GET"})
     * @IsGranted("ROLE_DC")
     */
    public function getCreatedAndNotValidatedConventions(ConventionRepository $conventionRepository): Response
    {
        return $this->render('convention/createdAndNotValidatedConventions.html.twig', [
            'createdConventions' => $conventionRepository->findBy(['currentState' => self::CREATED_TYPE]),
            'validatedConventions' => $conventionRepository->findBy(['currentState' => self::VALIDATED_TYPE]),
            'notValidatedConventions' => $conventionRepository->findBy(['currentState' => self::NOT_VALIDATED_TYPE])
        ]);
    }

    /**
     * @Route("/validatedAndPayedAndNotPayed", name="convention_index_validatedAndPayedAndNotPayed", methods={"GET"})
     * @IsGranted("ROLE_DF")
     */
    public function getValidatedAndPayedAndNotPayedConventions(ConventionRepository $conventionRepository): Response
    {
        return $this->render('convention/validatedAndPayedAndNotPayedConventions.html.twig', [
            'validatedConventions' => $conventionRepository->findBy(['currentState' => self::VALIDATED_TYPE]),
            'payedConventions' => $conventionRepository->findBy(['currentState' => self::PAYED_TYPE]),
            'notPayedConventions' => $conventionRepository->findBy(['currentState' => self::NOT_PAYED_TYPE])
        ]);
    }

    /**
     * @Route("/new", name="convention_new", methods={"GET","POST"})
     * @IsGranted("ROLE_DC")
     */
    public function new(Request $request, FileUploader $fileUploader): Response
    {
        $newDate = new \DateTime();
        $endDate = $newDate->add(new \DateInterval('P10D'));
        $endDate->setTime(0, 0);

        $newDate2 = new \DateTime();
        $endDate2 = $newDate2->add(new \DateInterval('P20D'));
        $endDate2->setTime(0, 0);

        $convention = new Convention();
        $convention->setSignatureDate(new \DateTime());
        $convention->setWorkStartDate($endDate);
        $convention->setWorkEndDate($endDate2);

        $form = $this->createForm(ConventionType::class, $convention);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //check dates
            if (!($convention->getWorkStartDate() > $convention->getSignatureDate())) {
                $this->addFlash(
                    'error',
                    'La date de début de travaux  doit être postérieure à la date de signature !'
                );
                return $this->redirectToRoute('convention_new');
            }
            if (!($convention->getWorkEndDate() > $convention->getWorkStartDate())) {
                $this->addFlash(
                    'error',
                    'La date de fin des travaux doit être postérieure à la date de début de travaux !'
                );
                return $this->redirectToRoute('convention_new');
            }

            // prepare entityManager
            $entityManager = $this->getDoctrine()->getManager();

            // set and persist $convention
            $convention->setCurrentState(self::CREATED_TYPE);
            $entityManager->persist($convention);

            // add files
            /** @var UploadedFile[] $uploadedFiles */
            $uploadedFiles = $form['attachments']->getData();

            if ($uploadedFiles) {
                foreach ($uploadedFiles as $uploadedFile) {
                    $file = $fileUploader->upload($uploadedFile);
                    $file->setConvention($convention);
                    $entityManager->persist($file);
                }
            }

            // set and persist $state
            /** @var User $user */
            $user = $this->getUser();

            $state = new State();
            $state->setType(self::CREATED_TYPE);
            $state->setDate(new \DateTime());
            $state->setUser($user);
            $state->setConvention($convention);
            $entityManager->persist($state);

            // flush
            $entityManager->flush();

            return $this->redirectToRoute('convention_index_createdAndNotValidated');
        }

        return $this->render('convention/new.html.twig', [
            'convention' => $convention,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="convention_show", methods={"GET"})
     * @Security("is_granted('ROLE_USER') or is_granted('ROLE_DC') or is_granted('ROLE_DF') or is_granted('ROLE_ADMIN')")
     */
    public function show(Convention $convention): Response
    {
        /** @var User $user */
        $user = $this->getUser();

        $canReviewed = (in_array('ROLE_USER', $user->getRoles())) &&
            ($convention->getCurrentState() === self::CREATED_TYPE) &&
            (!$convention->getAvailableDepartmentsToAddReviews()->isEmpty()) &&
            ($convention->getAvailableDepartmentsToAddReviews()->contains($user->getDepartment()));

        $canValidated = (in_array('ROLE_DC', $user->getRoles())) &&
            ($convention->getCurrentState() === self::CREATED_TYPE) &&
            (
                ($convention->getAvailableDepartmentsToAddReviews()->isEmpty()) ||
                ((!$convention->getAvailableDepartmentsToAddReviews()->isEmpty()) &&
                    (!$convention->getNotes()->isEmpty()))
            );

        $canPayed = (in_array('ROLE_DF', $user->getRoles())) &&
            ($convention->getCurrentState() === self::VALIDATED_TYPE);

        return $this->render('convention/show.html.twig', [
            'convention' => $convention,
            'canReviewed' => $canReviewed,
            'canValidated' => $canValidated,
            'canPayed' => $canPayed
        ]);
    }

    /**
     * @Route("/{id}/edit", name="convention_edit", methods={"GET","POST"})
     * @IsGranted("ROLE_DC")
     */
    public function edit(Request $request, Convention $convention): Response
    {
        $form = $this->createForm(ConventionEditType::class, $convention);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //check dates
            if (!($convention->getWorkStartDate() > $convention->getSignatureDate())) {
                $this->addFlash(
                    'error',
                    'La date de début de travaux  doit être postérieure à la date de signature !'
                );
                return $this->redirectToRoute('convention_edit', ['id' => $convention->getId()]);
            }
            if (!($convention->getWorkEndDate() > $convention->getWorkStartDate())) {
                $this->addFlash(
                    'error',
                    'La date de fin des travaux doit être postérieure à la date de début de travaux !'
                );
                return $this->redirectToRoute('convention_edit', ['id' => $convention->getId()]);
            }

            // save
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($convention);
            $entityManager->flush();

            return $this->redirectToRoute('convention_show', ['id' => $convention->getId()]);
        }

        return $this->render('convention/edit.html.twig', [
            'convention' => $convention,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="convention_delete", methods={"POST"})
     * @IsGranted("ROLE_DC")
     */
    public function delete(Request $request, Convention $convention): Response
    {
        if ($this->isCsrfTokenValid('delete' . $convention->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();

            foreach ($convention->getStates() as $state) {
                $entityManager->remove($state);
            }

            foreach ($convention->getFiles() as $file) {
                $entityManager->remove($file);
            }

            $entityManager->remove($convention);
            $entityManager->flush();
        }

        return $this->redirectToRoute('convention_index_createdAndNotValidated');
    }

    /**
     * @Route("/{id}/updatePayed", name="convention_edit_status_payed", methods={"GET","POST"})
     * @IsGranted("ROLE_DF")
     */
    public function updateConventionStatusPayed(Request $request, Convention $convention): Response
    {
        $form = $this->createForm(ConventionPaymentType::class, $convention);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $convention->setCurrentState(self::PAYED_TYPE);

            /** @var User $user */
            $user = $this->getUser();

            $state = new State();
            $state->setType(self::PAYED_TYPE);
            $state->setDate(new \DateTime());
            $state->setUser($user);
            $state->setConvention($convention);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($convention);
            $entityManager->persist($state);
            $entityManager->flush();

            return $this->redirectToRoute('convention_index_validatedAndPayedAndNotPayed');
        }

        return $this->render('convention/payed.html.twig', [
            'convention' => $convention,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/updateNotPayed", name="convention_edit_status_not_payed", methods={"GET"})
     * @IsGranted("ROLE_DF")
     */
    public function updateConventionStatusNotPayed(Convention $convention): Response
    {
        $convention->setCurrentState(self::NOT_PAYED_TYPE);

        /** @var User $user */
        $user = $this->getUser();

        $state = new State();
        $state->setType(self::NOT_PAYED_TYPE);
        $state->setDate(new \DateTime());
        $state->setUser($user);
        $state->setConvention($convention);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($convention);
        $entityManager->persist($state);
        $entityManager->flush();

        return $this->redirectToRoute('convention_index_validatedAndPayedAndNotPayed');
    }
}
