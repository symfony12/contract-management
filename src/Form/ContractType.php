<?php

namespace App\Form;

use App\Entity\Contract;
use App\Entity\Department;
use App\Entity\Provider;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContractType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('reference', TextType::class, [
                'label' => 'Référence',
                'required' => true
            ])
            ->add('marketReference', TextType::class, [
                'label' => 'Référence marché',
                'required' => true
            ])
            ->add('object', TextType::class, [
                'label' => 'Objet',
                'required' => false
            ])
            ->add('startDate', DateTimeType::class, [
                'label' => 'Entrée en vigueur',
                'required' => true
            ])
            ->add('endDate', DateTimeType::class, [
                'label' => 'Fin de contrat',
                'required' => true
            ])
            ->add('amount', NumberType::class, [
                'label' => 'Montant',
                'required' => true
            ])
            ->add('formalNotice', TextType::class, [
                'label' => 'Mise en demeure',
                'required' => false
            ])
            ->add('renewable', RadioType::class, [
                'label' => 'Renouvelable',
                'required' => false
            ])
            ->add('provider', EntityType::class, [
                'label' => 'Fournisseur',
                'class' => Provider::class,
                'choice_label' => 'name',
                'placeholder' => 'Sélectionnez',
                'required' => true
            ])
            ->add('availableDepartmentsToAddReviews', EntityType::class, [
                'label' => 'Directions',
                'class' => Department::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->where('c.id != ?1')
                        ->andWhere('c.id != ?2')
                        ->andWhere('c.id != ?3')
                        ->setParameter(1, 1)
                        ->setParameter(2, 2)
                        ->setParameter(3, 3);
                },
                'choice_label' => 'name',
                'multiple'=> true,
                'expanded'=> false,
                'required' => false
            ])
            ->add('attachments', FileType::class, [
                'label'=> 'Pièces jointes',
                'multiple'=> true,
                'required'=> false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Contract::class,
        ]);
    }
}
